Gitlab Issues
---------------------

 * Introduction
   The Gitlab Issue module displays a button on every page.
   To allow the users to create an issue using the Gitlab API.
   You can configure the module in the system settings
   (Admin -> config -> development -> gitlab issue).

 * For a full description of the module, visit the project page:
   https://drupal.org/project/gitlab-issue

 * Requirements
   This module requires the following modules:
     * Jquery update (https://drupal.org/project/jquery_update)
     * Ctools (https://drupal.org/project/ctools)

 * Installation
   * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

 * Configuration
   * Configure user permissions in
     Administration » People » Permissions:
     - Who can or may create issues?
   * Configure the API in Admin -> config -> development -> gitlab issues
     - Private key, can be found on
       https://gitlab.com/profile/account -> Private token
     - Project ID can be found by calling the API or go to
       Gitlab -> Project -> CI/DC pipelines, look at the CURL query and the key
       between the 'v3/projects/' and '/trigger' is your project id.
     - Assignee ID can be found by calling API.
     - Label -> Your chose.

 * Troubleshooting
   * If the Chaos tools modal does not load, check if the site
     had no Javascript errors.
   * If the issue is not posted, check if your keys are up to date.

 * Maintainers
   Current maintainers:
     * Dieter Martens (Dietermartens) - https://www.drupal.org/u/dietermartens
